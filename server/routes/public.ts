import { Router, Response, Request } from 'express';
let mysql = require('mysql');
let Sequelize = require('sequelize');
let moment = require('moment');

const publicRouter: Router = Router();

let dbParams = {
  host: 'mdbfree.cugcvlfj9hxz.us-east-1.rds.amazonaws.com',
  user: 'john',
  password: 'x26WrZbtiiQmYt'
};

let sequelizeOptions = {
  define: {
    freezeTableName: true,
    timestamps: false
  }
};

publicRouter.get('/simple', (request: Request, response: Response) => {
  console.log('in /api/public/simple');

  let sequelize = new Sequelize('mysql://john:x26WrZbtiiQmYt@mdbfree.cugcvlfj9hxz.us-east-1.rds.amazonaws.com/nfl', sequelizeOptions);

  let Game = sequelize.define('game', {
    gameId: {
      type: Sequelize.TEXT,
      field: 'gid',
      primaryKey: true
    },
    homeTeam: {
      type: Sequelize.STRING,
      field: 'h'
    },
    awayTeam: {
      type: Sequelize.STRING,
      field: 'v'
    }
  });

  Game.findAll({
    where: {
      seas: 2015,
      wk: 21
    }
  }).then(function(game){
    response.json(game);
  })

});

publicRouter.get('/zone/rush/averages/:position', (request: Request, response: Response) => {

  let position = request.params.position;
  let table;
  if (position == 'offense') {
    table = `rush_zone_team_off_yr`;
  } else if (position == 'defense') {
    table = `rush_zone_team_def_yr`;
  } else {
    response.status(400).json({'error': `Bad offense/defense value: '${position}', should be 'offense' or 'defense'`});
    return;
  }

  let query = `SELECT
                AVG(LE) as LE_AVG,
                STDDEV(LE) as LE_STD,
                AVG(LT) as LT_AVG,
                STDDEV(LT) as LT_STD,
                AVG(LG) as LG_AVG,
                STDDEV(LG) as LG_STD,
                AVG(MD) as MD_AVG,
                STDDEV(MD) as MD_STD,
                AVG(RG) as RG_AVG,
                STDDEV(RG) as RG_STD,
                AVG(RT) as RT_AVG,
                STDDEV(RT) as RT_STD,
                AVG(RE) as RE_AVG,
                STDDEV(RE) as RE_STD,
                AVG(total) as AVG,
                STDDEV(total) as STD
                 FROM
                 nfl_current.${table}`;

  let connection = mysql.createConnection(dbParams);
  connection.connect();
  queryDB(response, connection, query);
});

publicRouter.get('/games/:season/:week', (request: Request, response: Response) => {
  console.log('in /games/:season/:week');
  let connection = mysql.createConnection(dbParams);
  connection.connect();

  // let requestedWeekLastGame = `SELECT date
  //                               FROM nfl_current.schedule
  //                               WHERE seas = ${request.params.season} AND wk = ${request.params.week}
  //                               ORDER BY gid DESC LIMIT 1`;
  // let requestedGameDate: string = '';
  // connection.query(requestedWeekLastGame, function (error, rows) {
  //   let date = rows[0].date.split(' ')[0].split('/');
  //   let month = date[0];
  //   let day = date[1];
  //   let year = date[2];
  //   requestedGameDate = moment().set({'month': month - 1, 'date': day, 'year': year});
  //   console.log("LAST GAME DATE", requestedGameDate);
  //   let today = moment();
  //   console.log("TODAY", today);
  //   if (today.diff(requestedGameDate) > 0){
  //     console.log('the requested week end was before today');
  //   } else {
  //     console.log('the requested week end is after today');
  //   }
  // });

  let db = 'nfl';
  let table = 'game';
  let week = request.params.week;
  let season = request.params.season;
  if (season == '2016') {
    db = `${db}_current`;
  }
  let query = `
    SELECT
      v as visitor,
      h as home,
      ou as overUnder,
      sprv as spread,
      ptsv as visitorScore,
      ptsh as homeScore,
      temp as temperature,
      cond as weather,
      day as weekday,
      surf as surface,
      stad as stadium
        FROM ${db}.${table}
        WHERE seas = ${season}
        AND wk = ${week}
  `;
  queryDB(response, connection, query);
});

publicRouter.get('/zone/rush/team/yr/:position/2016/:team', (request: Request, response: Response, next) => {
  console.log('in /zone/rush/team/yr/:position/2016/:team');
  let position = request.params.position;
  let where, table;
  if (position == 'offense') {
    table = `rush_zone_team_off_yr`;
    where = 'offensive_team';
  } else if (position == 'defense') {
    table = `rush_zone_team_def_yr`;
    where = 'defensive_team';
  } else {
    response.status(400).json({'error': `Bad offense/defense value: '${position}', should be 'offense' or 'defense'`});
    return;
  }
  let db = 'nfl_current';
  let team = request.params.team;
  let connection = mysql.createConnection(dbParams);
  connection.connect();
  let query = `
    SELECT *
      FROM ${db}.${table}
        WHERE ${where} = '${team}'
      ORDER BY total DESC
  `;
  queryDB(response, connection, query);
});

function queryDB(response, connection, query) {
  connection.query(query, function (error, rows) {
    if (error) {
      connection.end(function(error){
        console.log(error);
      });
      response.status(500).json({'error': `Internal Service Error ${error}`});
    } else {
      connection.end(function(){
        console.log("Ending Connection...");
      });
      if (rows.length > 0) {
        response.status(200).json(rows);
      } else {
        response.status(404).json({'error': 'Resource Not Found'});
      }
    }
  });
}

export { publicRouter }

import {Component} from '@angular/core';

@Component({
  selector   : 'my-app',
  template: `<main>
              <app-header></app-header>
              <router-outlet></router-outlet>
             </main>
  `
})
export class AppComponent {
}

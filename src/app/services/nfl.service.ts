import {Http, Response} from "@angular/http";
import {Injectable} from "@angular/core";
import 'rxjs/Rx';
import {Observable} from "rxjs";
import {Game} from "../models/game.model";
import {TeamHelperService} from "./team-helper.service";
import {Team} from "../models/team.model";
import {RushingModel} from "../models/rushing-zone.model";

@Injectable()
export class NFLService {

  private games: Game[] = [];
  private activeGame: Game;
  private homeTeam: Team;
  private awayTeam: Team;

  setTeams(homeTeam: Team, awayTeam: Team){
    this.homeTeam = homeTeam;
    this.awayTeam = awayTeam;
  }

  getAwayTeam(){
    return this.awayTeam;
  }

  getHomeTeam(){
    return this.homeTeam;
  }

  setActiveGame(game: Game){
    this.activeGame = game;
  }

  getActiveGame(){
    return this.activeGame;
  }

  constructor(private http: Http, private teamHelper: TeamHelperService) {}

  getGames(season: string, week: string) {
    let url = `api/public/games/${season}/${week}`;
    return this.http.get(url)
      .map((response: Response) => {
        const games = response.json();
        let transformedGames: Game[] = [];
        for (let game of games) {
          let homeTeam = this.teamHelper.getTeamByNickname(game.home);
          let awayTeam = this.teamHelper.getTeamByNickname(game.visitor);
          transformedGames.push(new Game(
            awayTeam,
            homeTeam,
            game.overUnder,
            game.spread,
            game.visitorScore,
            game.homeScore,
            game.temperature,
            game.weather,
            game.weekday,
            game.surface,
            game.stadium));
        }
        this.games = transformedGames;
        return transformedGames;
      })
      .catch((error: Response) => Observable.throw(error.json()));
  }

  getRushingZoneDataByTeam(offenseOrDefense: string, team: string): Observable<RushingModel> {
    let url = `api/public/zone/rush/team/yr/${offenseOrDefense}/2016/${team}`;
    let teamNameColumn = 'offensive_team';
    if (offenseOrDefense == 'defense'){
      teamNameColumn = 'defensive_team';
    }
    return this.http.get(url)
      .map((response: Response) => {
        const rushingTotals = response.json()[0];
        return new RushingModel(

          this.teamHelper.getTeamByNickname(rushingTotals[teamNameColumn]),
          offenseOrDefense,
          rushingTotals.year,

          rushingTotals.RT,
          rushingTotals.LG,
          rushingTotals.LE,
          rushingTotals.MD,
          rushingTotals.RE,
          rushingTotals.LT,
          rushingTotals.RG,
          rushingTotals.total,

          rushingTotals.RTn,
          rushingTotals.LGn,
          rushingTotals.LEn,
          rushingTotals.MDn,
          rushingTotals.REn,
          rushingTotals.LTn,
          rushingTotals.RGn,
          rushingTotals.totaln
        )
      })
      .catch((error: Response) => Observable.throw(error.json()));
  }

  getLeagueAverages(offenseOrDefense: string){
    let url = `api/public/zone/rush/averages/${offenseOrDefense}`;
    return this.http.get(url)
      .map((response: Response) => {
        const averages = response.json()[0];
        let transformedAverages = {
          type: 'spline',
          name: `League Average`,
          data: [averages.LE_AVG, averages.LT_AVG, averages.LG_AVG, averages.MD_AVG, averages.RG_AVG, averages.RT_AVG, averages.RE_AVG]
        };
        return transformedAverages;
      })
      .catch((error: Response) => Observable.throw(error.json()));
  }
}

import {Injectable} from "@angular/core";

import {Team} from "../models/team.model";

@Injectable()
export class TeamHelperService {

  years = [
    {text: '2016'},
    {text: '2015'},
    {text: '2014'},
    {text: '2013'},
    {text: '2012'},
    {text: '2011'},
    {text: '2010'}
    ];
  weeks = [
    {text: "One", code: "1"},
    {text: "Two", code: "2"},
    {text: "Three", code: "3"},
    {text: "Four", code: "4"},
    {text: "Five", code: "5"},
    {text: "Six", code: "6"},
    {text: "Seven", code: "7"},
    {text: "Eight", code: "8"},
    {text: "Nine", code: "9"},
    {text: "Ten", code: "10"},
    {text: "Eleven", code: "11"},
    {text: "Twelve", code: "12"},
    {text: "Thirteen", code: "13"},
    {text: "Fourteen", code: "14"},
    {text: "Fifteen", code: "15"},
    {text: "Sixteen", code: "16"},
    {text: "Seventeen", code: "17"},
    {text: "Wild Card", code: "18"},
    {text: "Divisional Playoffs", code: "19"},
    {text: "Conference Championship", code: "20"},
    {text: "Superbowl", code: "21"},
  ];

  Teams: Team[] = [
    new Team('NE', 'New England', 'patriots.gif', '1', ['#C80815', '#0D254C', '#D6D6D6', '#FFFFFF']),
    new Team('MIA', 'Miami', 'dolphins.gif', '2', ['#008D97', '#F5811F']),
    new Team('NYJ', 'New York', 'jets.gif', '3', ['#0C371D', '#FFFFFF']),
    new Team('BUF', 'Buffalo', 'bills.gif', '4', ['#00338D', '#C60C30']),

    new Team('BAL', 'Baltimore', 'ravens.gif', '5', ['#280353', '#D0B240', '#000000']),
    new Team('CIN', 'Cincinnati', 'bengals.gif', '6', ['#FB4F14', '#000000']),
    new Team('CLE', 'Cleveland', 'browns.gif', '7', ['#512F2D', '#FE3C00']),
    new Team('PIT', 'Pittsburgh', 'steelers.gif', '8', ['#FFB612', '#000000', '#FFFFFF']),

    new Team('HOU', 'Houston', 'texans.gif', '9', ['#02253A', '#B31B34']),
    new Team('IND', 'Indianapolis', 'colts.gif', '10', ['#003B7B', '#FFFFFF']),
    new Team('JAC', 'Jacksonville', 'jaguars.gif', '11', ['#006778', '#D7A22A', '#9F792C', '#FFFFFF', '#000000']),
    new Team('TEN', 'Tennessee', 'titans.gif', '12', ['#648FCC', '#0D254C']),

    new Team('DEN', 'Denver', 'broncos.gif', '13', ['#FB4F14', '#002244']),
    new Team('KC', 'Kansas City', 'chiefs.gif', '14', ['#B20032', '#F2C800']),
    new Team('OAK', 'Oakland', 'raiders.gif', '15', ['#C4C8CB', '#000000']),
    new Team('SD', 'San Diego', 'chargers.gif', '16', ['#0072CE', '#FFB81C', '#0C2340']),

    new Team('CHI', 'Chicago', 'bears.gif', '17', ['#DD4814', '#03202F']),
    new Team('DET', 'Detroit', 'lions.gif', '18', ['#C5C7CF', '#006DB0', '#000000', '#FFFFFF']),
    new Team('GB', 'Green Bay', 'packers.gif', '19', ['#203731', '#FFB612']),
    new Team('MIN', 'Minnesota', 'vikings.gif', '20', ['#582C81', '#F0BF00']),

    new Team('ATL', 'Atlanta', 'falcons.gif', '21', ['#A6192D', '#000000']),
    new Team('CAR', 'Carolina', 'panthers.gif', '22', ['#0088CE', '#A5ACAF', '#000000']),
    new Team('NO', 'New Orleans', 'saints.gif', '23', ['#D2B887', '#000000']),
    new Team('TB', 'Tampa Bay', 'buccaneers.gif', '24', ['#D60A0B', '#89765F' ,'#000000']),

    new Team('ARI', 'Arizona', 'cardinals.gif', '25', ['#9B2743', '#000000']),
    new Team('STL', 'Los Angeles', 'rams.gif', '26', ['#C9AF74', '#13264B']),
    new Team('SF', 'San Francisco', '49ers.gif', '27', ['#E6BE8A', '#AF1E2C']),
    new Team('SEA', 'Seattle', 'seahawks.gif', '28', ['#69BE28', '#A5ACAF', '#002244']),

    new Team('DAL', 'Dallas', 'cowboys.gif', '29', ['#0D254C', '#87909B', '#FFFFFF']),
    new Team('NYG', 'New York', 'giants.gif', '30', ['#192F6B', '#CA001A', '#A2AAAD', '#FFFFFF']),
    new Team('PHI', 'Philadelphia', 'eagles.gif', '31', ['#003B48', '#C0C0C0', '#708090', '#000000']),
    new Team('WAS', 'Washington', 'redskins.gif', '32', ['#FFB612', '#773141', '#FFFFFF']),
  ];

  getTeamByNickname(name: string) {
    if (name == 'LA') {
      name = 'STL';
    }
    return this.Teams.filter(team => team.nickName == name)[0];
  }

  getTeamById(id: string) {
    return this.Teams.filter(team => team.id == id)[0];
  }
}

import {Injectable} from "@angular/core";
import 'rxjs/Rx';
import {RushingModel} from "../models/rushing-zone.model";

@Injectable()
export class ChartService {

  keys = ['Left End', 'Left Tackle', 'Left Guard', 'Middle', 'Right Guard', 'Right Tackle', 'Right End'];

  convertHexToRGBWithOpacity(hex, opacity): string {
    hex = hex.replace('#','');
    let r = parseInt(hex.substring(0,2), 16);
    let g = parseInt(hex.substring(2,4), 16);
    let b = parseInt(hex.substring(4,6), 16);
    return 'rgba('+r+','+g+','+b+','+opacity+')';
  }

  transformDataForColumnChart(rushingModel: RushingModel, rawOrNormalized: string): any {
    let legendColor = this.convertHexToRGBWithOpacity(rushingModel.team.colors[0], .9);
    let pointPadding = 0.15;
    let pointPlacement = 0;
    if (rushingModel.offenseOrDefense == 'offense'){
      legendColor = this.convertHexToRGBWithOpacity(rushingModel.team.colors[0], .7);
      pointPadding = 0.25;
    }
    let rawData = this.getModelValuesForDataType(rushingModel, 'raw');
    let normalizedData = this.getModelValuesForDataType(rushingModel, 'normalized');
    let chartData = {
      title: `${rushingModel.team.nickName} ${rushingModel.offenseOrDefense}`,
      categories: this.keys,
      series: [
        {
          raw: rawData,
          normalized: normalizedData,
          color: legendColor,
          name: `${rushingModel.team.nickName} ${rushingModel.offenseOrDefense}`,
          data: rawData,
          pointPadding: pointPadding,
          pointPlacement: pointPlacement
        }
      ]
    };
    if (rawOrNormalized == 'normalized'){
      chartData.series[0].data = normalizedData;
    }
    return chartData;
  }

  getMinNegativeDefensiveValue(rushingModel: RushingModel): number {
    let vals = this.getModelValuesForDataType(rushingModel, 'raw').sort(function (a, b) {
      let aNum = +a;
      let bNum = +b;
      return aNum - bNum;
    });
    return +vals[0];
  }

  getModelValuesForDataType(rushingModel: RushingModel, dataType: string){
    let vals = [];
    rushingModel.keys[dataType].map(function(e){
      vals.push(rushingModel[e.modelName]);
    });
    return vals;
  }

  transformDataForNVD3Chart(rushingModel: RushingModel): any{
    let mergedData = [];
    // let barColors = ['#b20000', '#cc0000', '#e50000', '#ff0000', '#e50000','#cc0000','#b20000'];
    let barColor = rushingModel.team.colors[0];
    let legendColor = barColor;
    if (rushingModel.offenseOrDefense == 'offense'){
      // barColors = ['#0000b2', '#0000cc', '#0000e5', '#0000ff', '#0000e5', '#0000cc', '#0000b2'];
      let barColor = rushingModel.team.colors[0];
      legendColor = barColor;
    }
    rushingModel.keys['raw'].map(function(e, i) {
      mergedData.push({x: e.chartName, y: rushingModel[e.modelName], color: barColor});
    });
    return [
      {
        color: legendColor,
        values: mergedData,
        key: `${rushingModel.team.nickName} ${rushingModel.offenseOrDefense}`
      }
    ][0];
  }
}

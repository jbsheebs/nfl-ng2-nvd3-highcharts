import {Routes, RouterModule} from '@angular/router';
import {TeamGridComponent} from "./teams/team-grid.component";
import {TeamDetailsComponent} from "./teams/team-details.component";
import {TeamsComponent} from "./teams/teams.component";
import {ModuleWithProviders} from "@angular/core";

const routes: Routes = [
  {path: '', redirectTo: 'teams', pathMatch: 'full'},
  {path: 'teams', component: TeamsComponent,
    children: [
      {path: '', redirectTo: 'team-grid', pathMatch: 'full'},
      {path: 'team-grid', component: TeamGridComponent},
      {path: 'team-details/:id', component: TeamDetailsComponent }
    ]
  },
  {path: 'matchups', loadChildren: 'app/matchups/matchups.module#MatchupsModule'}
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);

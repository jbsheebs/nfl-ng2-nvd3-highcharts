
import {Team} from "./team.model";

export class Game {
    constructor(
        public visitor: Team,
        public home: Team,
        public overUnder: string,
        public spread: string,
        public visitorScore: string,
        public homeScore: string,
        public temperature: string,
        public weather: string,
        public weekday: string,
        public surface: string,
        public stadium: string
    ) {}

}

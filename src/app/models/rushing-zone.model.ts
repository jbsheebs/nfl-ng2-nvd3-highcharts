import {Team} from "./team.model";
export class RushingModel {

  constructor(

    public team: Team,
    public offenseOrDefense: string,
    public year: string,

    public leftEnd: string,
    public leftTackle: string,
    public leftGuard: string,
    public middle: string,
    public rightGuard: string,
    public rightTackle: string,
    public rightEnd: string,

    public total: string,

    public leftEndNormalized?: string,
    public leftTackleNormalized?: string,
    public leftGuardNormalized?: string,
    public middleNormalized?: string,
    public rightGuardNormalized?: string,
    public rightTackleNormalized?: string,
    public rightEndNormalized?: string,

    public totalNormalized?: string,

  ) {}

  keys = {
    raw: [
      {chartName: 'Left End', modelName: 'leftEnd'},
      {chartName: 'Left Tackle', modelName: 'leftTackle'},
      {chartName: 'Left Guard', modelName: 'leftGuard'},
      {chartName: 'Middle', modelName: 'middle'},
      {chartName: 'Right Guard', modelName: 'rightGuard'},
      {chartName: 'Right Tackle', modelName: 'rightTackle'},
      {chartName: 'Right End', modelName: 'rightEnd'}
    ],
    normalized: [
      {chartName: 'Left End', modelName: 'leftEndNormalized'},
      {chartName: 'Left Tackle', modelName: 'leftTackleNormalized'},
      {chartName: 'Left Guard', modelName: 'leftGuardNormalized'},
      {chartName: 'Middle', modelName: 'middleNormalized'},
      {chartName: 'Right Guard', modelName: 'rightGuardNormalized'},
      {chartName: 'Right Tackle', modelName: 'rightTackleNormalized'},
      {chartName: 'Right End', modelName: 'rightEndNormalized'}
    ]
  };

  getNegativeValues(): RushingModel{
    return new RushingModel(

      this.team,
      this.offenseOrDefense,
      this.year,

      '-' + this.leftEnd,
      '-' + this.leftTackle,
      '-' + this.leftGuard,
      '-' + this.middle,
      '-' + this.rightGuard,
      '-' + this.rightTackle,
      '-' + this.rightEnd,
      '-' + this.total,

    );
  }
}


import {RushingModel} from "./rushing-zone.model";
export class Team {

  public offensiveRushingModel: RushingModel;
  public defensiveRushingModel: RushingModel;

  constructor(
    public nickName: string,
    public fullName: string,
    public imagePath: string,
    public id: string,
    public colors?: string[]
  ) {}

  setOffensiveRushingModel(model: RushingModel) {
    this.offensiveRushingModel = model;
  }

  setDefensiveRushingModel(model: RushingModel) {
    this.defensiveRushingModel = model;
  }

  getOffensiveRushingModel(): RushingModel {
    return this.offensiveRushingModel;
  }

  getDefensiveRushingModel(): RushingModel {
    return this.defensiveRushingModel;
  }

}

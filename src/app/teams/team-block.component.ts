import {Component, OnInit, Input} from '@angular/core';

import {Team} from "../models/team.model";

@Component({
  selector: 'team-block',
  template: `
    <div class="team-block-wrap" [routerLink]="['../team-details', team.id]">
      <div class="team-info-wrap">
        <span class="team-name">{{team.fullName}}</span>
        <img class="team-image" [src]=teamImagePath />
      </div>
    </div>
    `,
  styles: [`
    .team-block-wrap {
    width: 100%;
    height: 100px;
    border: 2px solid black;
    margin: 10px 0;
}
.team-info-wrap {
  margin-top: 5px;
  text-align: center;
  width: 140px;
  height: 100%;
}
.team-image {
  display: block;
  margin: auto;
  height: 60px;
}
.team-block-wrap:hover {
  cursor: pointer;
  overflow: hidden;
  opacity: .8;
  font-size: 16px;
  font-weight: bolder;
  border: 3px solid black;
  box-shadow: 5px 5px 5px #888888;
}
`]

})
export class TeamBlockComponent implements OnInit {

  @Input() team: Team;
  partialPath: string = '../nfl-teams/';
  teamImagePath: string;

  ngOnInit(): void {
    this.teamImagePath = `${this.partialPath}${this.team.imagePath}`;
  }
}

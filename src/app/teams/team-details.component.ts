import {Component, OnInit} from '@angular/core';

import {Team} from "../models/team.model";
import {ActivatedRoute} from "@angular/router";
import {TeamHelperService} from "../services/team-helper.service";


@Component({
  selector: 'team-details',
  template: `
    <div class="rushing-chart-content-wrap">
      <div class="offensive-chart-wrap chart-wrap">
        <rushing-chart class="offensive-chart" [localTeam]="team" [defenseOrOffense]="offense"></rushing-chart>
      </div>
      <div class="defensive-chart-wrap chart-wrap">
        <rushing-chart class="defensive-chart" [localTeam]="team" [defenseOrOffense]="defense"></rushing-chart>
      </div>
    </div>
  `,
  styles: [`
    .rushing-chart-content-wrap{
      max-width: 1200px;
    }
    .chart-wrap{
      padding: 15px 15px 15px 10px;
      background: white;
      border: 1px solid darkgray;
      box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
    }
    .offensive-chart-wrap {  
      display: block;
    }
    .defensive-chart-wrap {
      display: block;
      margin-top: 25px;
    }
  `]
})
export class TeamDetailsComponent implements OnInit {

  team: Team;
  private sub: any;
  private id: string;
  private defense = 'defense';
  private offense = 'offense';

  constructor(private activatedRoute: ActivatedRoute,
              private teamHelperService: TeamHelperService) {}

  ngOnInit() {
    this.sub = this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
    });
    this.team = this.teamHelperService.getTeamById(this.id);
  }


}

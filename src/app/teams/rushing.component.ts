import {Component, OnInit, Input} from '@angular/core';
import {NFLService} from "../services/nfl.service";
import {Team} from "../models/team.model";
import {ChartService} from "../services/chart.service";
import {TeamHelperService} from "../services/team-helper.service";
declare let d3: any;

import * as Highcharts from 'highcharts';
import * as HighchartsExporting from 'highcharts/modules/exporting';
import {Observable} from "rxjs";

HighchartsExporting(Highcharts);
@Component({
  selector: 'rushing-chart',
  templateUrl: 'rushing.component.html'
})
export class RushingComponent implements OnInit {

  constructor(public nflService: NFLService,
              public chartService: ChartService,
              public teamHelperService: TeamHelperService) {}

  highchartOptions: Object;
  @Input() localTeam: Team;
  @Input() defenseOrOffense: string;
  opponents: Team[];
  activeOpponent;
  chartData;
  originalChartTitle;
  originalChartDataSeries;
  rawOrNormalized;
  initialized;
  leagueAverages;

  ngOnInit() {
    this.initialized = false;
    this.activeOpponent = "-1";
    this.rawOrNormalized = 'raw';
    this.opponents = this.teamHelperService.Teams.filter(team => team.fullName !== this.localTeam.fullName);
    Observable.forkJoin(
      this.getRushingDataForThisTeam(this.localTeam.nickName),
      this.nflService.getLeagueAverages(this.defenseOrOffense)
    ).subscribe(
      data => {
        this.chartData = this.chartService.transformDataForColumnChart(data[0], this.rawOrNormalized);
        this.originalChartTitle = this.chartData.title;
        this.originalChartDataSeries = this.chartData.series;
        this.leagueAverages = data[1];
        this.chartData.series.push(this.leagueAverages);
        this.drawChart();
      }
    )
  }

    // this.nflService.getLeagueAverages(this.defenseOrOffense)
    //   .subscribe(data => {
    //     console.log(data);
    //   });

    // this.getRushingDataForThisTeam(this.localTeam.nickName)
    //   .subscribe(teamData => {
    //     this.chartData = this.chartService.transformDataForColumnChart(teamData, this.rawOrNormalized);
    //     this.originalChartTitle = this.chartData.title;
    //     this.drawChart();
    //   });

  changeData(){
    console.log(this.rawOrNormalized);
    console.log(this.chartData);
    this.swapRawOrNormalizedData();
    this.drawChart();
  }

  swapRawOrNormalizedData(){
    if (this.rawOrNormalized == 'normalized'){
      this.chartData.series.pop();
      for (let data of this.chartData.series) {
        data.data = data.normalized;
      }
    } else if (this.rawOrNormalized == 'raw'){
      for (let data of this.chartData.series){
        data.data = data.raw;
      }
      this.chartData.series.push(this.leagueAverages);
    }
  }

  getRushingDataForThisTeam(team: string){
    if (this.defenseOrOffense == 'offense'){
      return this.nflService.getRushingZoneDataByTeam('offense', team);
    } else {
      return this.nflService.getRushingZoneDataByTeam('defense', team);
    }
  }

  getRushingDataForOpponent(team: string){
    if (this.defenseOrOffense == 'defense'){
      return this.nflService.getRushingZoneDataByTeam('offense', team);
    } else {
      return this.nflService.getRushingZoneDataByTeam('defense', team);
    }
  }

  onChangeOpponent() {
    this.getRushingDataForOpponent(this.activeOpponent.nickName)
      .subscribe(teamData => {
        let opponentData = this.chartService.transformDataForColumnChart(teamData, this.rawOrNormalized);
        this.chartData.title = `${this.originalChartTitle} vs. ${opponentData.title}`;
        this.addOpponentToExistingChartData(opponentData);
        this.drawChart();
      });
  }

  addOpponentToExistingChartData(opponentData: any){
    // add defense first
    if (this.defenseOrOffense == 'offense') {
      this.chartData.series = [opponentData.series[0], this.originalChartDataSeries[0]];
    } else {
      this.chartData.series = [this.originalChartDataSeries[0], opponentData.series[0]];
    }
    if (this.rawOrNormalized == 'raw') {
      this.chartData.series.push(this.leagueAverages);
    }

    // if (this.defenseOrOffense == 'offense') {
    //   if (this.chartData.series.length !== 1) {
    //     this.chartData.series.splice(0, 1);
    //   }
    //   this.chartData.series.unshift(opponentData.series[0]);
    // } else {
    //   this.chartData.series.splice(1, 1);
    //   this.chartData.series.push(opponentData.series[0]);
    // }
  }

  displayChartOptions() {
    console.log('SHOW OPTIONS...');
  }

  drawChart() {
    // HIGHCHARTS
    this.highchartOptions = {
      credits: {
        enabled: false
      },
      chart: {
        type: 'column'
      },
      exporting: {
        buttons: {
          contextButton: {
            align: 'left'
          }
        }
      },
      title: {
        text: this.chartData.title,
        margin: 40
      },
      xAxis: {
        categories: this.chartData.categories,
        title: {
          text: 'Rushing Direction',
          y: 10
        }
      },
      yAxis: {
        allowDecimals: false,
        title: {
          text: 'Total Yards'
        },
        stackLabels: {
          enabled: true,
          style: {
            fontWeight: 'bold',
            color: 'gray'
          }
        }
      },
      legend: {
        align: 'right',
        x: -30,
        verticalAlign: 'top',
        y: 25,
        floating: true,
        backgroundColor: 'white',
        borderColor: '#CCC',
        borderWidth: 1,
        shadow: false
      },
      tooltip: {
        shared: true
      },
      plotOptions: {
        column: {
          grouping: false,
          shadow: false,
          borderWidth: 0
        }
      },
      series: this.chartData.series
    };
  }
}

import {Component} from '@angular/core';

@Component({
  selector: 'teams',
  template: `
  <div class="team-page-content-wrap">
    <router-outlet></router-outlet>
  </div>
`,
  styles: [`
    .team-page-content-wrap{
      position: absolute;
      left: 265px;
      right: 30px;
      top: 75px;
      background-color: #f5f5f5;
    -webkit-transition: all 500ms ease;
    -moz-transition: all 500ms ease;
    -ms-transition: all 500ms ease;
    -o-transition: all 500ms ease;
    transition: all 500ms ease;
    }
    @media only screen and (max-width: 850px) {
      .team-page-content-wrap {
        left: 30px;
      }
    }
    @media only screen and (max-width: 600px) {
      .team-page-content-wrap {
        left: 5px !important;
        right: 5px !important;
      }
    }
  `]
})
export class TeamsComponent {

}

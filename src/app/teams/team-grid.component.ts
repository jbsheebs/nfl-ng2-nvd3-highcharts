import {Component, OnInit} from '@angular/core';

import {TeamHelperService} from "../services/team-helper.service";
import {Team} from "../models/team.model";

@Component({
  selector: 'team-grid',
  template: `
  <div class="team-block-wrap">
    <team-block [team]="team" *ngFor="let team of teams"></team-block>
  </div>
  `,
  styles: [`

  `]
})
export class TeamGridComponent implements OnInit {

  constructor(private teamHelper: TeamHelperService) {}

  teams: Team[];

  ngOnInit() {
    this.teams = this.teamHelper.Teams;
  }
}

import {MatchupsComponent} from "./matchups.component";
import {GridComponent} from "./grid.component";
import {ChartContainerComponent} from "./chart.component";
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: MatchupsComponent,
    children: [
      {path: '', redirectTo: 'matchup-list', pathMatch: 'full'},
      {path: 'matchup-list', component: GridComponent},
      {path: 'matchup-details', component: ChartContainerComponent}
      ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

import {Component, OnInit} from '@angular/core';

import {NFLService} from "../services/nfl.service";
import {Game} from "../models/game.model";
import {TeamHelperService} from "../services/team-helper.service";

@Component({
  selector: 'nfl-grid',
  templateUrl: './grid.component.html'
})
export class GridComponent implements OnInit {

  constructor(public nflService: NFLService,
              private teamHelper: TeamHelperService) {}

  nflGames:Game[] = [];
  validGame = true;

  years = this.teamHelper.years;
  weeks = this.teamHelper.weeks;

  activeWeek;
  activeYear;

  onChange(timeUnit: any){
    console.log(timeUnit);
    this.getGames(this.activeYear.text, this.activeWeek.code);
  }

  ngOnInit() {
    this.activeWeek = this.weeks[0];
    this.activeYear = this.years[0];
    this.getGames(this.activeYear.text, this.activeWeek.code);
  }

  getGames(year: string, week: string){
    this.nflService.getGames(year, week)
      .subscribe(
        games => {
          this.nflGames = games;
          this.validGame = true;
        },
        error => {
          console.log(error);
          this.validGame = false;
        });
  }
}

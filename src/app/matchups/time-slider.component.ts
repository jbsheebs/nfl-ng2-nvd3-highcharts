import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import any = jasmine.any;

@Component({
  selector: 'time-slider',
  template: `
  <div class="time-unit-select">
    <div (click)="centerAndShift(unit)" *ngFor="let unit of timeUnitContainer" class="week-block" [ngClass]="unit.css">
      <div class="week-text">{{unit.text}}</div>
    </div>
  </div>
  `,
  styles: [`
  .time-unit-select{
    margin: 10px auto;
    position: absolute;
    top: 60px;
    width: 500px;
    height: 50px;
    border-radius: 4px;
    // border: 1px solid #cccccc;
    overflow: hidden;
    background-image: linear-gradient(to right, #7a7a7a -100px, white 400px, #7a7a7a 900px);
    // background: linear-gradient(to left, gray, white 25%, white 75%, gray 100%);
    background-size: 800px 100%;
    background-position: 50% 100%;
    background-repeat: no-repeat;
    box-shadow: 0 0 2px 1px #b7b7b7;
  }
  .hide-right{
    left: 110%;
    letter-spacing: -1px;
  }
  .hide-left{
    left: -20%;
    letter-spacing: -1px;
  }
  .week-block{
    cursor: pointer;
    margin: 10px;
    font-size: 16px;
    position: absolute;
    transition: ease all 333ms;
  }
  .far-left{
    left: 0;
    letter-spacing: -1px;
  }
  .mid-left{
    left: 20%;
    letter-spacing: -1px;
  }
  .middle{
    left: 44%;
    letter-spacing: 0;
    font-weight: bold;
  }
  .mid-right{
    left: 68%;
    letter-spacing: -1px;
  }
  .far-right{
    left: 88%;
    letter-spacing: -1px;
  }
  `]
})
export class TimeSlider implements OnInit {

  ngOnInit(): void {
    this.setUpWeeksForInitialDisplay();
  }

  constructor() {}

  @Input() currentIndex;
  @Input() timeUnitContainer;
  @Output() selectTick = new EventEmitter<any>();

  setUpWeeksForInitialDisplay(){
    this.setVisibleItems(this.currentIndex);
  }

  setVisibleItems(i){
    this.timeUnitContainer[i].css = 'middle';
    let max, min = i;
    if (this.timeUnitContainer[i-1]){
      min = i-1;
      this.timeUnitContainer[i-1].css = 'mid-left';
    }
    if (this.timeUnitContainer[i-2]){
      min = i-2;
      this.timeUnitContainer[i-2].css = 'far-left';
    }
    if (this.timeUnitContainer[i+1]){
      max = i+1;
      this.timeUnitContainer[i+1].css = 'mid-right';
    }
    if (this.timeUnitContainer[i+2]){
      max = i+2;
      this.timeUnitContainer[i+2].css = 'far-right';
    }
    for (let x = 0; x < min; x++){
      this.timeUnitContainer[x].css = "hide-left";
    }
    for (let x = max + 1; x < this.timeUnitContainer.length; x++){
      this.timeUnitContainer[x].css = "hide-right";
    }
  }

  centerAndShift(selectedWeek){
    this.setVisibleItems(this.timeUnitContainer.indexOf(selectedWeek));
    this.selectTick.emit(selectedWeek.code);
  }

}

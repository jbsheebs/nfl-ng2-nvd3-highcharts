import {Component, OnInit, Input} from '@angular/core';
import {NFLService} from "../services/nfl.service";
import {Observable} from "rxjs";
import {Team} from "../models/team.model";
import {ChartService} from "../services/chart.service";
declare let d3, nv: any;

@Component({
  selector: 'offensive-rush',
  template: `<div><nvd3 [options]="options" [data]="data"></nvd3></div>`
})
export class NVComponent implements OnInit {

  options;
  data;
  minY: number;
  @Input() offensiveTeam: Team;
  @Input() defensiveTeam: Team;

  constructor(public nflService: NFLService, public chartService: ChartService) {}

  getOffensiveRushingData(team: string){
    return this.nflService.getRushingZoneDataByTeam('offense', team);
  }

  getDefensiveRushingData(team: string){
    return this.nflService.getRushingZoneDataByTeam('defense', team);
  }

  ngOnInit(){
    Observable.forkJoin(
      this.getOffensiveRushingData(this.offensiveTeam.nickName),
      this.getDefensiveRushingData(this.defensiveTeam.nickName)
    ).subscribe(
      data => {
        let offensiveData = data[0];
        let defensiveData = data[1];
        this.offensiveTeam.setOffensiveRushingModel(offensiveData);
        this.defensiveTeam.setDefensiveRushingModel(defensiveData);
        let tempData = [
          this.chartService.transformDataForNVD3Chart(offensiveData),
          this.chartService.transformDataForNVD3Chart(defensiveData.getNegativeValues())
        ];
        console.log('TEMPDATA', tempData);
        this.minY = this.chartService.getMinNegativeDefensiveValue(defensiveData.getNegativeValues());
        this.options = {
          chart: {
            forceY: this.minY,
            // I like multiBarHorizontalChart better I think
            type: 'multiBarChart',
            height: 400,
            margin: {
              top: 50,
              right: 20,
              bottom: 50,
              left: 100
            },
            x: function (d) {
              return d.x;
            },
            y: function (d) {
              return d.y;
            },
            showValues: true,
            valueFormat: function (d) {
              return d3.format(',.0f')(d);
            },
            duration: 500,
            xAxis: {
              axisLabel: 'Rushing Zone',
              margin: {
                left: 100
              }
            },
            yAxis: {
              axisLabel: 'Total Yards',
              axisLabelDistance: -10
            }
          }
        };
        this.data = tempData;
        console.log(this.minY);
      }
    );
  }
}

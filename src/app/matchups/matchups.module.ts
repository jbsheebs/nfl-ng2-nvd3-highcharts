import { NgModule } from '@angular/core';
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {HttpModule} from "@angular/http";

import {routing} from "./matchups.routing";
import {ChartContainerComponent} from "./chart.component";
import {GridComponent} from "./grid.component";
import {GridRowComponent} from "./grid-row.component";
import {MatchupsComponent} from "./matchups.component";
import {NVComponent} from "./nv.component";
import {nvD3} from "ng2-nvd3";

@NgModule({
  imports: [
    routing,
    FormsModule,
    CommonModule,
    HttpModule
  ],
  declarations: [
    ChartContainerComponent,
    GridComponent,
    GridRowComponent,
    MatchupsComponent,
    NVComponent,
    nvD3
  ]

})

export class MatchupsModule {}

import {Component} from '@angular/core';

@Component({
  selector: 'matchups',
  template: `<router-outlet></router-outlet>`
})
export class MatchupsComponent {

}

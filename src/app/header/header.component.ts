import {Component} from "@angular/core";

@Component({
    selector: 'app-header',
    templateUrl: 'header.component.html'
})
export class HeaderComponent {

  isMobileMenuOpen = false;

  hideMobileMenu() {
    this.isMobileMenuOpen = false;
    document.getElementById('root').classList.remove('mobile-menu-open')
  }

  openMobileMenu() {
    if (this.isMobileMenuOpen === false) {
      this.isMobileMenuOpen = true;
      document.getElementById('root').classList.add('mobile-menu-open');
    } else {
      this.isMobileMenuOpen = false;
      document.getElementById('root').classList.remove('mobile-menu-open');
    }
  }
}

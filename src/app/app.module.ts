import {NgModule} from '@angular/core'
import {routing} from "./app.routing";
import {AppComponent} from "./app.component";
import {FormsModule} from "@angular/forms";
import {BrowserModule} from "@angular/platform-browser";
import {HttpModule} from "@angular/http";
import {GameDetailsComponent} from './about/about.component';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import {HeaderComponent} from './header/header.component';
import {NFLService} from "./services/nfl.service";
import {TeamHelperService} from "./services/team-helper.service";
import {TeamGridComponent} from "./teams/team-grid.component";
import {TeamBlockComponent} from "./teams/team-block.component";
import {TeamDetailsComponent} from "./teams/team-details.component";
import {RushingComponent} from "./teams/rushing.component";
import {ChartModule} from "angular2-highcharts";
import {ChartService} from "./services/chart.service";
import {TeamsComponent} from "./teams/teams.component";

@NgModule({
  declarations: [
    AppComponent,
    GameDetailsComponent,
    HeaderComponent,
    TeamGridComponent,
    TeamBlockComponent,
    TeamDetailsComponent,
    RushingComponent,
    TeamsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ChartModule,
    HttpModule,
    routing
  ],
  providers   : [
    NFLService,
    TeamHelperService,
    ChartService,
    {provide: LocationStrategy,
      useClass: HashLocationStrategy}
  ],
  bootstrap   : [AppComponent]
})
export class AppModule {

}

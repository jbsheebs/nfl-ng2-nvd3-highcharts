- NFL Application with Express Server, Angular2 Client leveraging NVD3 and Highcharts
- Angular 2 ( 2.x )
- ExpressJS ( 4.x - with compression )
- Webpack ( angular-cli )
- Smart & dumb components
- Advanced routing ( lazy loading, router outlets...)

## Install / Development

```bash
git clone https://jbsheebs@bitbucket.org/jbsheebs/nfl-ng2-nvd3-highcharts.git
cd nfl-ng2-nvd3-highcharts

# Install dependencies
npm install

# start server
npm run start

# Client url: http://localhost:4200
# Application ( Express ) API: http://localhost:4300
```

## Build / Production

```bash

npm run build

## Deploy dist folder to app server

Structure of dist folder:

/dist/server <-- expressjs
/dist/client <-- angular2

```

